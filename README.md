# qmesh

Welcome the the qmesh code development repository.

### What is qmesh?

qmesh is a software package for creating high-quality meshes using [QGIS](http://www.qgis.org) and [Gmsh](http://geuz.org/gmsh).
The meshes can be used in finite element numerical models such as [TELEMAC](http://www.opentelemac.org), [Fluidity](http://www.fluidity-project.org) and [Thetis](http://thetisproject.org/).
For more information please visit the project [web-site](http://www.qmesh.org).


### Configuration & Installation

Please see the relevant [wiki page](https://bitbucket.org/qmesh-developers/qmesh/wiki/Installation).

### Testing

To ensure that QMesh has been installed properly and that it is operating correctly, you can run the test suite by running:

```
make test
```

## Contributors

The lead developers of qmesh are listed in file [AUTHORS.md](./AUTHORS.md)

## License

QMesh is available under the [GNU General Public License](http://www.gnu.org/copyleft/gpl.html). Please see the file [LICENSE](./LICENSE) for more information.
