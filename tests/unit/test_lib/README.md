#    Copyright (C) 2013 Alexandros Avdis and others. See the AUTHORS file for a full list of copyright holders.
#
#    This file is part of QMesh.
#
#    QMesh is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    QMesh is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with QMesh.  If not, see <http://www.gnu.org/licenses/>.

Directory containing unit-tests for the lib module of qmesh.

You can invoke all tests in this directory by:
```
python -m unittest discover
```

You can invoke test-cases in this directory as follows:
```
./ <filename>
```
where `<filename>` is composed as `test_<qmesh class>.py`

You can invoke an individual test routine as follows:
```
python -m unittest <test-case>.<routine>
```
where `<test-case>` is the filename without the `.py` extension.
