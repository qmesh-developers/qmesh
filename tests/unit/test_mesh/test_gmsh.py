#!/usr/bin/env python

#    Copyright (C) 2013 Alexandros Avdis and others. See the AUTHORS file for a full list of copyright holders.
#
#    This file is part of QMesh.
#
#    QMesh is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    QMesh is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with QMesh.  If not, see <http://www.gnu.org/licenses/>.

import unittest
# so we import local python before any other
#import sys
#sys.path.insert(0,"../../../")
#sys.path.insert(0,"../")
import qmesh

class TestGGeometryPoints(unittest.TestCase):

    def setUp(self):
        qmesh.LOG.setLevel('WARNING')
        qmesh.initialise()

    # this should fail, the point is only 2D
    def test_add_point(self):
        myGeometry = qmesh.mesh.Geometry()
        try:
            myGeometry.addPoint([0,1])
        except AssertionError:
            self.assert_(True)
            return
        self.assert_(False)

    # again, should fail as we're passing dodgy data
    def test_add_incorrect_point(self):
        myGeometry = qmesh.mesh.Geometry()
        try:
            myGeometry.addPoint(1)
        except AssertionError:
            self.assert_(True)
            return
        self.assert_(False)

    # again, should fail as we're passing dodgy data
    def test_add_point_as_string(self):
        myGeometry = qmesh.mesh.Geometry()
        try:
            myGeometry.addPoint("hhh")
        except AssertionError:
            self.assert_(True)
            return
        self.assert_(False)

    def test_add_existing_point(self):
        myGeometry = qmesh.mesh.Geometry()
        myGeometry.addPoint([0,1,1])
        myGeometry.addPoint([0,1,1])
        self.assert_(myGeometry.points == {0:[0,1,1], 1:[0,1,1]})

    def test_remove_existing_point(self):
        myGeometry = qmesh.mesh.Geometry()
        myGeometry.addPoint([0,1,1])
        myGeometry.removePoint(0)
        self.assert_(myGeometry.points == {})

    def test_remove_nonexisting_point(self):
        myGeometry = qmesh.mesh.Geometry()
        myGeometry.addPoint([0,1,1])
        try:
            myGeometry.removePoint(1)
        except AssertionError:
            self.assert_(True)
            return
        self.assert_(False)
        
    def test_get_point(self):
        myGeometry = qmesh.mesh.Geometry()
        myGeometry.addPoint([0,1,1])
        myGeometry.addPoint([0,1,2])
        self.assert_(myGeometry.getPoint(0) == [0,1,1])
        self.assert_(myGeometry.getPoint(1) == [0,1,2])
        try:
            self.assert_(myGeometry.getPoint(2))
        except AssertionError:
            self.assert_(True)
            return
        self.assert_(False)

    def test_get_point_odd_input(self):
        myGeometry = qmesh.mesh.Geometry()
        myGeometry.addPoint([0,1,1])
        myGeometry.addPoint([0,1,2])
        try:
            self.assert_(myGeometry.getPoint("ibiv"))
        except AssertionError:
            self.assert_(True)
            return
        self.assert_(False)

    def test_add_points_from_dict(self):
        myGeometry = qmesh.mesh.Geometry()
        points = {0: [0,1,0], 1:[1,1,2], 2:[3,54,1]}
        myGeometry.addPointsFromDictionary(points)
        self.assert_(myGeometry.getPoint(0) == [0,1,0])

    def test_add_points_from_dict_existing(self):
        myGeometry = qmesh.mesh.Geometry()
        myGeometry.addPoint([0,1,1])
        points = {0: [0,1,0], 1:[1,1,2], 2:[3,54,1]}
        myGeometry.addPointsFromDictionary(points)
        self.assert_(myGeometry.getPoint(0) == [0,1,1])
        self.assert_(myGeometry.pointsCount() == 4)

    def test_add_points_existing(self):
        myGeometry = qmesh.mesh.Geometry()
        myGeometry.addPoint([0,1,1])
        points = [[0,1,0], [1,1,2], [3,54,1]]
        myGeometry.addPoints(points)
        self.assert_(myGeometry.getPoint(0) == [0,1,1])
        self.assert_(myGeometry.pointsCount() == 4)


class TestGGeometryLines(unittest.TestCase):

    def setUp(self):
        qmesh.LOG.setLevel('WARNING')
        qmesh.initialise()

    def test_addLineSegment(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        G.addLineSegment([0,1])
        self.assert_(G.lineSegmentsCount() == 1)

    def test_addLineSegment_missingPoint(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        try:
            G.addLineSegment([0,10])
        except AssertionError:
            self.assert_(True)
            return

        self.assert_(False)

    def test_remove_LineSegment(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        G.addLineSegment([0,1])
        G.removeLineSegment(0)
        self.assert_(G.lineSegmentsCount() == 0)
        try:
            G.removeLineSegment(0)
        except AssertionError:
            self.assert_(True)
            return
        self.assert_(False)

    def test_getLineSegment(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        G.addLineSegment([0,1])
        self.assert_(G.lineSegmentsCount() == 1)
        line = G.getLineSegment(0)
        self.assert_(line == [0,1])


    def test_get_nonexistant_LineSegment(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        G.addLineSegment([0,1])
        self.assert_(G.lineSegmentsCount() == 1)
        try:
            G.getLineSegment(1)
        except AssertionError:
            self.assert_(True)
            return
        self.assert_(False)

    def test_add_lines_dictionary(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        lines = {0: [0,1], 1:[1,2], 2:[2,3]}
        G.addLineSegmentsFromDictionary(lines)
        self.assert_(G.lineSegmentsCount() == 3)



class TestGGeometryBSplines(unittest.TestCase):

    def setUp(self):
        qmesh.LOG.setLevel('WARNING')
        qmesh.initialise()

    def test_addBspline(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        G.addBspline([0,1,2])
        self.assert_(G.bsplinesCount() == 1)

    def test_addBspline_missingPoint(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        try:
            G.addBspline([0,10,1])
        except AssertionError:
            self.assert_(True)
            return
        self.assert_(False)

    def test_remove_Bspline(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        G.addBspline([0,1,2])
        G.removeBspline(0)
        self.assert_(G.bsplinesCount() == 0)
        try:
            G.removeLineSegment(0)
        except AssertionError:
            self.assert_(True)
            return
        self.assert_(False)

    def test_getBspline(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        G.addBspline([0,1,2])
        self.assert_(G.bsplinesCount() == 1)
        line = G.getBspline(0)
        self.assert_(line == [0,1,2])


    def test_get_nonexistant_Bspline(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        G.addBspline([0,1,2])
        self.assert_(G.bsplinesCount() == 1)
        try:
            G.getBspline(1)
        except AssertionError:
            self.assert_(True)
            return
        self.assert_(False)

    def test_add_Bspline_dictionary_nonexisting(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        lines = {0: [0,1,4], 1:[1,2,3]}
        try:
            G.addBsplinesFromDictionary(lines)
        except AssertionError:
            self.assert_(True)
        self.assert_(False)

    def test_add_Bspline_dictionary_nonexisting(self):
        G = qmesh.mesh.Geometry()
        G.addPoints([[0,0,0], [0,1,0], [1,1,0], [1,0,0]])
        lines = {0: [0,1,2], 1:[1,2,3]}
        try:
            G.addBsplinesFromDictionary(lines)
        except AssertionError:
            self.assert_(False)
        self.assert_(G.bsplinesCount() == 2)
        


if __name__ == '__main__':
    unittest.main()



