#!/usr/bin/env python

#    Copyright (C) 2013 Alexandros Avdis and others. See the AUTHORS file for a full list of copyright holders.
#
#    This file is part of QMesh.
#
#    QMesh is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    QMesh is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with QMesh.  If not, see <http://www.gnu.org/licenses/>.

import unittest
# so we import local python before any other
#import sys
#sys.path.insert(0,"../../../")
#import os.path
import qmesh
print qmesh.__file__

class Test_Raster(unittest.TestCase):

    def setUp(self):
        import os
        qmesh.LOG.setLevel('WARNING')
        qmesh.initialise()
        self.thisPath = os.path.dirname(os.path.realpath(__file__))
        self.raster_filename = os.path.join(self.thisPath,'test_raster.ascii')
        self.fld_filename = os.path.join(self.thisPath,"test.fld")

    def test_write_fld(self):
        """ Test writing of fld files. """
        raster = qmesh.raster.meshMetricTools.raster()
        raster.fromFile(self.raster_filename)
        targetCoordReferenceSystem = "EPSG:4326"
        raster.writefld(self.fld_filename,targetCoordReferenceSystem)
        # now test the output
        # read as a string
        with open(self.fld_filename,'r') as f:
            fld = f.read()
        # check no 'nan' present
        self.assert_(not 'nan' in fld)
        # check no sci notation
        self.assert_(not 'e' in fld)
        # check contains 0.001
        self.assert_('0.0001' in fld)
        # check contains 0.000 (nans and 1e-10
        self.assert_('0.0000' in fld)
        # check coords are correct
        self.assert_('0.0 0.0 0' in fld)
        self.assert_('1.0 1.0 1' in fld)
        self.assert_('10 10 1' in fld)





suite = unittest.TestLoader().loadTestsFromTestCase(Test_Raster)

if __name__ == '__main__':
    unittest.main()
