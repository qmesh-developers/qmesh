#!/usr/bin/env python

#    Copyright (C) 2013 Alexandros Avdis and others. See the AUTHORS file for a full list of copyright holders.
#
#    This file is part of QMesh.
#
#    QMesh is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    QMesh is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with QMesh.  If not, see <http://www.gnu.org/licenses/>.

import unittest
# so we import local python before any other
#import sys
#sys.path.insert(0,"../../../")
#import os.path
import qmesh

class Test_Shapes(unittest.TestCase):

    def setUp(self):
        import os
        qmesh.LOG.setLevel('WARNING')
        qmesh.initialise()
        self.thisPath = os.path.dirname(os.path.realpath(__file__))
        self.multiLines_filename = self.thisPath+'/multiLines.shp'
        self.lines_filename = self.thisPath+'/lines.shp'
        self.multiPolygons_filename = self.thisPath+'/multiPolygons.shp'
        self.polygons_filename = self.thisPath+'/polygons.shp'

    def test_decomposeMultiLines(self):
        """ Test successful conversion of multilines into lines. """
        lines = qmesh.vector.Shapes()
        lines.fromFile(self.multiLines_filename)
        lines.decomposeMultiFeatures()
        lines.writeFile(self.lines_filename)
        
    def test_decomposeMultiPolygons(self):
        """ Test successful conversion of multipolygons into polygons. """
        polygons = qmesh.vector.Shapes()
        polygons.fromFile(self.multiPolygons_filename)
        polygons.decomposeMultiFeatures()
        polygons.writeFile(self.polygons_filename)

suite = unittest.TestLoader().loadTestsFromTestCase(Test_Shapes)

if __name__ == '__main__':
    unittest.main()
