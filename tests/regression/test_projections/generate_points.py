import numpy as np
import qgis.core
import PyQt4.QtCore

def generate_pointsShapefile(points, outputFilename, geoCS_WKT):
    '''Todo: add docstring'''
    qgis.core.QgsApplication.setPrefixPath('/usr', True)
    qgis.core.QgsApplication.initQgis()

    coordinateReferenceSystem = qgis.core.QgsCoordinateReferenceSystem()
    coordinateReferenceSystem.createFromWkt(geoCS_WKT)
    assert coordinateReferenceSystem.isValid()

    fields =  qgis.core.QgsFields()
    fields.append( qgis.core.QgsField("ID", PyQt4.QtCore.QVariant.Int) )

    writer = qgis.core.QgsVectorFileWriter(outputFilename, "CP1250", fields, qgis.core.QGis.WKBPoint, coordinateReferenceSystem, "ESRI Shapefile")
    if writer.hasError() != qgis.core.QgsVectorFileWriter.NoError:
        print "Error when creating shapefile: ", writer.hasError()

    #
    feature = qgis.core.QgsFeature()
    feature.setFields(fields)
    for point in points:
        QGIS_Point = qgis.core.QgsPoint(point[0],point[1])
        feature.setGeometry(qgis.core.QgsGeometry.fromPoint(QGIS_Point))
        feature.setAttribute("ID",0)
        writer.addFeature(feature)

    #delete the writer to flush features to disk (optional)
    del writer
