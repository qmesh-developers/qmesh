from generate_points import generate_pointsShapefile
import qgis.core
import os
import glob
import sys
import numpy as np
sys.path.append('../')
from readShapefile import readShapefile


def test_WGS84_2_polarStereographic():
    qgis.core.QgsApplication.setPrefixPath('/usr', True)
    qgis.core.QgsApplication.initQgis()
    #Create points in shapefile
    #sourceCRS_WKT = 'GEOGCS["Normal Sphere (r=6.37101e+6)",DATUM["WGS 84",SPHEROID["sphere",6.37101e+6,0]],PRIMEM["Greenwich",0],UNIT["degree",0.01745329251994328]]'
    sourceCRS_WKT = 'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]]'
    #sourceCRS_WKT = 'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6.37101e+6,0,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]]'
    generate_pointsShapefile([[0,90],
                              [-135.0,0],[-90,0],[-45,0],[0,0],[45,0],[90,0],[135.0,0],[180,0],
                             ],
                              'test_points.shp', sourceCRS_WKT)
    #Read shapefile
    sourceCRS, pointVectorLayer = readShapefile('test_points.shp',0)
    assert sourceCRS.isValid()
    #Initialise target projection
    targetCRS = qgis.core.QgsCoordinateReferenceSystem()
    #targetCRS.createFromWkt('PROJCS["unnamed",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6.37101e+6,0,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],PROJECTION["Polar_Stereographic"],PARAMETER["latitude_of_origin",90],PARAMETER["central_meridian",90],UNIT["Meter",1]]')
    #targetCRS.createFromWkt('PROJCS["unnamed",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],PROJECTION["Polar_Stereographic"],PARAMETER["latitude_of_origin",90],PARAMETER["central_meridian",90],UNIT["Meter",1]]')
    targetCRS.createFromWkt('PROJCS["unnamed",GEOGCS["Normal Sphere (r=0.5)",DATUM["unknown",SPHEROID["sphere",0.5,0]],PRIMEM["Greenwich",0],UNIT["degree",0.01745329251994328]],PROJECTION["Polar_Stereographic"],PARAMETER["latitude_of_origin",90],PARAMETER["central_meridian",90],UNIT["Meter",1]]')
    #targetCRS.createFromWkt('PROJCS["unnamed",GEOGCS["Normal Sphere (r=0.5)",DATUM["unknown",SPHEROID["sphere",6.37101e+6,0]],PRIMEM["Greenwich",0],UNIT["degree",0.01745329251994328]],PROJECTION["Polar_Stereographic"],PARAMETER["latitude_of_origin",90],PARAMETER["central_meridian",90],UNIT["Meter",1]]')
    #Project each point and check result for correctness.
    CoordTransformer = qgis.core.QgsCoordinateTransform(sourceCRS, targetCRS)
    CoordTransformer.initialise()
    assert CoordTransformer.isInitialised()
    assert not CoordTransformer.isShortCircuited()
    sCRS_points=[]
    tCRS_points=[]
    features  = pointVectorLayer.getFeatures()
    for feature in features:
        sCRS_point = feature.geometry().asPoint()
        sCRS_points.append(sCRS_point)
        tCRS_points.append(CoordTransformer.transform(sCRS_point))
    #Check each projected point for correctness.
    for sCRS_point, tCRS_point in zip(sCRS_points, tCRS_points):
        longitude = sCRS_point.x()
        latitude = sCRS_point.y()
        expected_ksi = -np.tan(np.radians(-latitude/2 + 45))*np.cos(np.radians(longitude))
        expected_eta = -np.tan(np.radians(-latitude/2 + 45))*np.sin(np.radians(longitude))
        ksi = tCRS_point.x()
        eta = tCRS_point.y()
        ksiError = abs(expected_ksi - ksi)
        etaError = abs(expected_eta - eta)
        assert -1e-9 < ksiError < 1e-9
        assert -1e-9 < etaError < 1e-9
    #Clean-up
    unwantedFiles = glob.glob('test_points.*')
    for unwantedFile in unwantedFiles:
        os.remove(unwantedFile)
