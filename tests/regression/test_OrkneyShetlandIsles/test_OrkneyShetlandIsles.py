#!/usr/bin/env python

#    Copyright (C) 2013 Alexandros Avdis and others. See the AUTHORS file for a full list of copyright holders.
#
#    This file is part of QMesh.
#
#    QMesh is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    QMesh is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with QMesh.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import numpy as np
import qgis.core
#import sys
#sys.path.append('../../../')
import qmesh
#qmesh.LOG.setLevel('ERROR')

class TestOrkneyShetlandIsles(unittest.TestCase):
    '''Todo: add test documentation as class docstring '''
    def test_OrkneyShetlandIsles_UTM30(self):
        '''Todo: add docstring '''
        #Try initialising qgis API
        try:
            qmesh.initialise()
            self.assert_(True)
        except AssertionError:
            self.assert_(False)
        #Try reading in the shapefile describing the domain boundaries, and creating a gmsh file.
        try:
            boundaries = qmesh.vector.Shapes()
            boundaries.fromFile('OrkneyShetlandIsles_singleRegion.shp')
            loopShapes = qmesh.vector.identifyLoops(boundaries,
                      isGlobal=False, defaultPhysID=1000,
                      fixOpenLoops=True)
            polygonShapes = qmesh.vector.identifyPolygons(loopShapes, smallestNotMeshedArea=50000)
            #Create loops and polygons for Inner Sound
            innerSound_plot_lines = qmesh.vector.Shapes()
            innerSound_plot_lines.fromFile('innerSound_plot_lines.shp')
            innerSound_plot_loops = qmesh.vector.identifyLoops(innerSound_plot_lines,
                                                               fixOpenLoops=True)
            innerSound_plot_polygon = qmesh.vector.identifyPolygons(innerSound_plot_loops, 
                                                                    meshedAreaPhysID = 2)
            #Create raster for mesh gradation towards full-resolution shorelines.
            GSHHS_fine_boundaries = qmesh.vector.Shapes()
            GSHHS_fine_boundaries.fromFile('GSHHS_f_L1_lines.shp')
            gradationRaster_shoreline = qmesh.raster.gradationToShapes()
            gradationRaster_shoreline.setShapes(GSHHS_fine_boundaries)
            gradationRaster_shoreline.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_shoreline.setRasterResolution(800,800)
            gradationRaster_shoreline.setGradationParameters(150.0,15000.0,1.0)
            gradationRaster_shoreline.calculateLinearGradation()
            gradationRaster_shoreline.writeNetCDF('gradation_to_GSHHS_f_lines.nc')
            #Create raster for mesh gradation towards Shetlands shorelines.
            shetlands_shorelines = qmesh.vector.Shapes()
            shetlands_shorelines.fromFile('Shetlands_shoreline_gebco08.shp')
            gradationRaster_shetlands_shoreline = qmesh.raster.gradationToShapes()
            gradationRaster_shetlands_shoreline.setShapes(shetlands_shorelines)
            gradationRaster_shetlands_shoreline.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_shetlands_shoreline.setRasterResolution(800,800)
            gradationRaster_shetlands_shoreline.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_shetlands_shoreline.calculateLinearGradation()
            gradationRaster_shetlands_shoreline.writeNetCDF('gradation_to_Shetlands_shoreline.nc')
            #Create raster for mesh gradation towards 0m gebco contour on the Scottish mainland coast
            GEBCO08_0mContour = qmesh.vector.Shapes()
            GEBCO08_0mContour.fromFile('GEBCO08_0mContour.shp')
            gradationRaster_GEBCO08_0mContour = qmesh.raster.gradationToShapes()
            gradationRaster_GEBCO08_0mContour.setShapes(GEBCO08_0mContour)
            gradationRaster_GEBCO08_0mContour.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_GEBCO08_0mContour.setRasterResolution(800,800)
            gradationRaster_GEBCO08_0mContour.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_GEBCO08_0mContour.calculateLinearGradation()
            gradationRaster_GEBCO08_0mContour.writeNetCDF('gradationRaster_GEBCO08_0mContour.nc')
            #Create raster for mesh gradation towards GSHHS high-resolution lines on the Scottish mainland coast
            GSHHS_h_L1_lines = qmesh.vector.Shapes()
            GSHHS_h_L1_lines.fromFile('GSHHS_h_L1_lines.shp')
            gradationRaster_GSHHS_h_L1_lines = qmesh.raster.gradationToShapes()
            gradationRaster_GSHHS_h_L1_lines.setShapes(GSHHS_h_L1_lines)
            gradationRaster_GSHHS_h_L1_lines.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_GSHHS_h_L1_lines.setRasterResolution(800,800)
            gradationRaster_GSHHS_h_L1_lines.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_GSHHS_h_L1_lines.calculateLinearGradation()
            gradationRaster_GSHHS_h_L1_lines.writeNetCDF('GSHHS_h_L1_lines_gradationRaster_.nc')
            #Create raster for mesh gradation towards Inner Sound
            gradationRaster_innerSound_plot = qmesh.raster.gradationToShapes()
            gradationRaster_innerSound_plot.setShapes(innerSound_plot_polygon)
            gradationRaster_innerSound_plot.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_innerSound_plot.setRasterResolution(800,800)
            gradationRaster_innerSound_plot.setGradationParameters(10.0,15000.0,1.0,0.01)
            gradationRaster_innerSound_plot.calculateLinearGradation()
            gradationRaster_innerSound_plot.writeNetCDF('gradation_to_InnerSoundPlot.nc')
            #Calculate overall mesh-metric raster
            meshMetricRaster = qmesh.raster.minimumRaster([gradationRaster_shoreline, \
                                                           gradationRaster_shetlands_shoreline, \
                                                           gradationRaster_GEBCO08_0mContour, \
                                                           gradationRaster_GSHHS_h_L1_lines, \
                                                           gradationRaster_innerSound_plot])
            meshMetricRaster.writeNetCDF('OrkneyShetlandIsles_UTM30_meshMetric.nc')
            domain = qmesh.mesh.Domain()
            domain.setGeometry(loopShapes, polygonShapes)
            domain.setMeshMetricField(meshMetricRaster)
            domain.setTargetCoordRefSystem('EPSG:32630', fldFillValue=1000.0)
        except AssertionError:
            self.assert_(False)
        #Try meshing
        try:
            domain.gmsh(geoFilename='OrkneyShetlandIsles_UTM30.geo', \
                        fldFilename='OrkneyShetlandIsles_UTM30.fld', \
                        mshFilename='OrkneyShetlandIsles_UTM30.msh')
        except AssertionError:
            self.assert_(False)

    def test_OrkneyShetlandIsles_PCC(self):
        '''Todo: add docstring '''
        #Try initialising qgis API
        try:
            qmesh.initialise()
            self.assert_(True)
        except AssertionError:
            self.assert_(False)
        #Try reading in the shapefile describing the domain boundaries, and creating a gmsh file.
        try:
            boundaries = qmesh.vector.Shapes()
            boundaries.fromFile('OrkneyShetlandIsles_singleRegion.shp')
            loopShapes = qmesh.vector.identifyLoops(boundaries,
                      isGlobal=False, defaultPhysID=1000,
                      fixOpenLoops=True)
            polygonShapes = qmesh.vector.identifyPolygons(loopShapes, smallestNotMeshedArea=50000)
            #Create loops and polygons for Inner Sound
            innerSound_plot_lines = qmesh.vector.Shapes()
            innerSound_plot_lines.fromFile('innerSound_plot_lines.shp')
            innerSound_plot_loops = qmesh.vector.identifyLoops(innerSound_plot_lines,
                                                               fixOpenLoops=True)
            innerSound_plot_polygon = qmesh.vector.identifyPolygons(innerSound_plot_loops, 
                                                                    meshedAreaPhysID = 2)
            #Create raster for mesh gradation towards full-resolution shorelines.
            GSHHS_fine_boundaries = qmesh.vector.Shapes()
            GSHHS_fine_boundaries.fromFile('GSHHS_f_L1_lines.shp')
            gradationRaster_shoreline = qmesh.raster.gradationToShapes()
            gradationRaster_shoreline.setShapes(GSHHS_fine_boundaries)
            gradationRaster_shoreline.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_shoreline.setRasterResolution(800,800)
            gradationRaster_shoreline.setGradationParameters(150.0,15000.0,1.0)
            gradationRaster_shoreline.calculateLinearGradation()
            gradationRaster_shoreline.writeNetCDF('gradation_to_GSHHS_f_lines.nc')
            #Create raster for mesh gradation towards Shetlands shorelines.
            shetlands_shorelines = qmesh.vector.Shapes()
            shetlands_shorelines.fromFile('Shetlands_shoreline_gebco08.shp')
            gradationRaster_shetlands_shoreline = qmesh.raster.gradationToShapes()
            gradationRaster_shetlands_shoreline.setShapes(shetlands_shorelines)
            gradationRaster_shetlands_shoreline.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_shetlands_shoreline.setRasterResolution(800,800)
            gradationRaster_shetlands_shoreline.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_shetlands_shoreline.calculateLinearGradation()
            gradationRaster_shetlands_shoreline.writeNetCDF('gradation_to_Shetlands_shoreline.nc')
            #Create raster for mesh gradation towards 0m gebco contour on the Scottish mainland coast
            GEBCO08_0mContour = qmesh.vector.Shapes()
            GEBCO08_0mContour.fromFile('GEBCO08_0mContour.shp')
            gradationRaster_GEBCO08_0mContour = qmesh.raster.gradationToShapes()
            gradationRaster_GEBCO08_0mContour.setShapes(GEBCO08_0mContour)
            gradationRaster_GEBCO08_0mContour.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_GEBCO08_0mContour.setRasterResolution(800,800)
            gradationRaster_GEBCO08_0mContour.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_GEBCO08_0mContour.calculateLinearGradation()
            gradationRaster_GEBCO08_0mContour.writeNetCDF('gradationRaster_GEBCO08_0mContour.nc')
            #Create raster for mesh gradation towards GSHHS high-resolution lines on the Scottish mainland coast
            GSHHS_h_L1_lines = qmesh.vector.Shapes()
            GSHHS_h_L1_lines.fromFile('GSHHS_h_L1_lines.shp')
            gradationRaster_GSHHS_h_L1_lines = qmesh.raster.gradationToShapes()
            gradationRaster_GSHHS_h_L1_lines.setShapes(GSHHS_h_L1_lines)
            gradationRaster_GSHHS_h_L1_lines.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_GSHHS_h_L1_lines.setRasterResolution(800,800)
            gradationRaster_GSHHS_h_L1_lines.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_GSHHS_h_L1_lines.calculateLinearGradation()
            gradationRaster_GSHHS_h_L1_lines.writeNetCDF('GSHHS_h_L1_lines_gradationRaster_.nc')
            #Create raster for mesh gradation towards Inner Sound
            gradationRaster_innerSound_plot = qmesh.raster.gradationToShapes()
            gradationRaster_innerSound_plot.setShapes(innerSound_plot_polygon)
            gradationRaster_innerSound_plot.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_innerSound_plot.setRasterResolution(800,800)
            gradationRaster_innerSound_plot.setGradationParameters(10.0,15000.0,1.0,0.01)
            gradationRaster_innerSound_plot.calculateLinearGradation()
            gradationRaster_innerSound_plot.writeNetCDF('gradation_to_InnerSoundPlot.nc')
            #Calculate overall mesh-metric raster
            meshMetricRaster = qmesh.raster.minimumRaster([gradationRaster_shoreline, \
                                                           gradationRaster_shetlands_shoreline, \
                                                           gradationRaster_GEBCO08_0mContour, \
                                                           gradationRaster_GSHHS_h_L1_lines, \
                                                           gradationRaster_innerSound_plot])
            meshMetricRaster.writeNetCDF('OrkneyShetlandIsles_PCC_meshMetric.nc')
            domain = qmesh.mesh.Domain()
            domain.setGeometry(loopShapes, polygonShapes)
            domain.setMeshMetricField(meshMetricRaster)
            domain.setTargetCoordRefSystem('PCC', fldFillValue=1000.0)
        except AssertionError:
            self.assert_(False)
        #Try meshing
        try:
            domain.gmsh(geoFilename='OrkneyShetlandIsles_PCC.geo', \
                        fldFilename='OrkneyShetlandIsles_PCC.fld', \
                        mshFilename='OrkneyShetlandIsles_PCC.msh')
        except AssertionError:
            self.assert_(False)

    def test_OrkneyShetlandIsles_PCC_tidalSites(self):
        '''Todo: add docstring '''
        #Try initialising qgis API
        try:
            qmesh.initialise()
            self.assert_(True)
        except AssertionError:
            self.assert_(False)
        #Try reading in the shapefile describing the domain boundaries, and creating a gmsh file.
        try:
            boundaries = qmesh.vector.Shapes()
            boundaries.fromFile('OrkneyShetlandIsles_singleRegion.shp')
            boundaries.writeInfo()
            loopShapes = qmesh.vector.identifyLoops(boundaries,
                      isGlobal=False, defaultPhysID=3000,
                      fixOpenLoops=True, extraPointsPerVertex=3)
            polygonShapes = qmesh.vector.identifyPolygons(loopShapes,
                                                          smallestNotMeshedArea=50000, 
                                                          meshedAreaPhysID = 1)
            #Create loops and polygons for Inner Sound tidal plot
            innerSound_plot_lines = qmesh.vector.Shapes()
            innerSound_plot_lines.fromFile('innerSound_plot_lines.shp')
            innerSound_plot_loops = qmesh.vector.identifyLoops(innerSound_plot_lines,
                                                               fixOpenLoops=True)
            innerSound_plot_polygon = qmesh.vector.identifyPolygons(innerSound_plot_loops, 
                                                                    meshedAreaPhysID = 2)
            #Insert tidal plots and turbines into domain.
            domainLines, domainPolygons = \
               qmesh.vector.insertRegions(
                  loopShapes, polygonShapes,\
                  innerSound_plot_loops, innerSound_plot_polygon)
            #Create raster for mesh gradation towards full-resolution shorelines.
            GSHHS_fine_boundaries = qmesh.vector.Shapes()
            GSHHS_fine_boundaries.fromFile('GSHHS_f_L1_lines.shp')
            gradationRaster_shoreline = qmesh.raster.gradationToShapes()
            gradationRaster_shoreline.setShapes(GSHHS_fine_boundaries)
            gradationRaster_shoreline.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_shoreline.setRasterResolution(800,800)
            gradationRaster_shoreline.setGradationParameters(150.0,15000.0,0.5)
            gradationRaster_shoreline.calculateLinearGradation()
            gradationRaster_shoreline.writeNetCDF('gradationRaster_shoreline.nc')
            #Create raster for mesh gradation towards Shetlands shorelines.
            shetlands_shorelines = qmesh.vector.Shapes()
            shetlands_shorelines.fromFile('Shetlands_shoreline_gebco08.shp')
            gradationRaster_shetlands_shoreline = qmesh.raster.gradationToShapes()
            gradationRaster_shetlands_shoreline.setShapes(shetlands_shorelines)
            gradationRaster_shetlands_shoreline.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_shetlands_shoreline.setRasterResolution(800,800)
            gradationRaster_shetlands_shoreline.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_shetlands_shoreline.calculateLinearGradation()
            gradationRaster_shetlands_shoreline.writeNetCDF('gradation_to_Shetlands_shoreline.nc')
            #Create raster for mesh gradation towards 0m gebco contour on the Scottish mainland coast
            GEBCO08_0mContour = qmesh.vector.Shapes()
            GEBCO08_0mContour.fromFile('GEBCO08_0mContour.shp')
            gradationRaster_GEBCO08_0mContour = qmesh.raster.gradationToShapes()
            gradationRaster_GEBCO08_0mContour.setShapes(GEBCO08_0mContour)
            gradationRaster_GEBCO08_0mContour.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_GEBCO08_0mContour.setRasterResolution(800,800)
            gradationRaster_GEBCO08_0mContour.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_GEBCO08_0mContour.calculateLinearGradation()
            gradationRaster_GEBCO08_0mContour.writeNetCDF('gradationRaster_GEBCO08_0mContour.nc')
            #Create raster for mesh gradation towards GSHHS high-resolution lines on the Scottish mainland coast
            GSHHS_h_L1_lines = qmesh.vector.Shapes()
            GSHHS_h_L1_lines.fromFile('GSHHS_h_L1_lines.shp')
            gradationRaster_GSHHS_h_L1_lines = qmesh.raster.gradationToShapes()
            gradationRaster_GSHHS_h_L1_lines.setShapes(GSHHS_h_L1_lines)
            gradationRaster_GSHHS_h_L1_lines.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_GSHHS_h_L1_lines.setRasterResolution(800,800)
            gradationRaster_GSHHS_h_L1_lines.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_GSHHS_h_L1_lines.calculateLinearGradation()
            gradationRaster_GSHHS_h_L1_lines.writeNetCDF('GSHHS_h_L1_lines_gradationRaster_.nc')
            #Create raster for mesh gradation towards Inner Sound
            gradationRaster_innerSound_plot = qmesh.raster.gradationToShapes()
            gradationRaster_innerSound_plot.setShapes(innerSound_plot_polygon)
            gradationRaster_innerSound_plot.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_innerSound_plot.setRasterResolution(800,800)
            gradationRaster_innerSound_plot.setGradationParameters(10.0,15000.0,2.0,0.05)
            gradationRaster_innerSound_plot.calculateLinearGradation()
            gradationRaster_innerSound_plot.writeNetCDF('gradationRaster_innerSound_plot.nc')
            #Calculate overall mesh-metric raster
            meshMetricRaster = qmesh.raster.minimumRaster([gradationRaster_shoreline, \
                                                           gradationRaster_shetlands_shoreline, \
                                                           gradationRaster_GEBCO08_0mContour, \
                                                           gradationRaster_GSHHS_h_L1_lines, \
                                                           gradationRaster_innerSound_plot])
            meshMetricRaster.writeNetCDF('meshMetric.nc')
            #Create domain object and write gmsh files.
            domain = qmesh.mesh.Domain()
            domain.setGeometry(domainLines, domainPolygons)
            domain.setMeshMetricField(meshMetricRaster)
            domain.setTargetCoordRefSystem('PCC', fldFillValue=1000.0)
        except AssertionError:
            self.assert_(False)
        #Try meshing
        try:
            domain.gmsh(geoFilename='OrkneyShetlandIsles_PCC_tidalSites.geo', \
                        fldFilename='OrkneyShetlandIsles_PCC_tidalSites.fld', \
                        mshFilename='OrkneyShetlandIsles_PCC_tidalSites.msh')
        except AssertionError:
            self.assert_(False)

    def test_OrkneyShetlandIsles_PCC_InnerSoundTurbines(self):
        '''Todo: add docstring '''
        #Try initialising qgis API
        try:
            qmesh.initialise()
            self.assert_(True)
        except AssertionError:
            self.assert_(False)
        #Try reading in the shapefile describing the domain boundaries, and creating a gmsh file.
        try:
            boundaries = qmesh.vector.Shapes()
            boundaries.fromFile('OrkneyShetlandIsles_singleRegion.shp')
            boundaries.writeInfo()
            loopShapes = qmesh.vector.identifyLoops(boundaries,
                      isGlobal=False, defaultPhysID=3000,
                      fixOpenLoops=True,
                      extraPointsPerVertex=3)
            polygonShapes = qmesh.vector.identifyPolygons(loopShapes, smallestNotMeshedArea=50000, 
                                                                             meshedAreaPhysID = 1)
            #Create loop and polygon for Inner Sound.
            innerSound_plot_lines = qmesh.vector.Shapes()
            innerSound_plot_lines.fromFile('innerSound_plot_lines.shp')
            innerSound_plot_loops = qmesh.vector.identifyLoops(innerSound_plot_lines,
                      fixOpenLoops=True, extraPointsPerVertex=10)
            innerSound_plot_polygon = qmesh.vector.identifyPolygons(innerSound_plot_loops, 
                                                                             meshedAreaPhysID = 2)
            innerSound_plot_polygon.writeFile('innerSound_plot_polygon.shp')
            #Using a random-number generator to distribute points inside
            # the Inner Sound plot, with the additional constrains that
            # they must be 200m apart. Locate 180 such points. Then create
            # a cicle for each point, representative of a tidal turbine.
            #Seed the random-number generator so as to obtain same set of
            # points every time the test is run. 
            np.random.seed([1.0,0.5])
            #Calculate mapping
            # parameters so as to map the interval [0,1] to an area
            # enclosing the tidal plot. The numbers below are corner
            # coordinates from a rectangle -in UTM30- that ecloses
            # the tidal plot
            ksi_param_1 = 494800 - 490400
            ksi_param_2 = 490400
            eta_param_1 = 6503800 - 6501600
            eta_param_2 = 6501600
            turbinePoints = []
            #Change the coordinate reference system of the Inner Sound plot
            # from lon-lat into UTM30 .
            innerSound_plot_polygon.changeCoordRefSystem('EPSG:32630')
            polygon = innerSound_plot_polygon.getFeatures()[0]
            while len(turbinePoints) < 180:
                #Get new "arbitrary" point, carry out mapping using
                # the parameters calculated above.
                newPointX = np.random.random()*ksi_param_1 + ksi_param_2
                newPointY = np.random.random()*eta_param_1 + eta_param_2
                #Construct a large circle around that point, 15m in radius
                # Then check if it is included into the tidal plot. The larger
                # radius should eliminate the possibility of the centre-point
                # of the circle lying inside the polygon, but too close to the
                # plot boundary such that a part of the tidal-turbine is
                # outside the plot.
                turbineBigCircle = qmesh.vector.Circle(newPointX, newPointY, 15.0, 20, 'EPSG:32630')
                turbineBigPolygon = turbineBigCircle.asQgsPolygonFeature()
                if polygon.geometry().contains(turbineBigPolygon.geometry()):
                    if len(turbinePoints) == 0:
                        turbinePoints.append((newPointX, newPointY))
                    else:
                        isNewPointAdmissible = True
                        for otherPoint in turbinePoints:
                            distance = np.sqrt( (otherPoint[0] - newPointX)**2 + (otherPoint[1] - newPointY)**2)
                            if distance < 100.0:
                                isNewPointAdmissible = False
                                break
                        if isNewPointAdmissible:
                            turbinePoints.append((newPointX, newPointY))
            turbines = qmesh.vector.Shapes()
            turbines.setCoordRefSystemFromString('EPSG:32630')
            turbines.setShapeType(qgis.core.QGis.WKBLineString)
            for turbinePoint in turbinePoints:
                turbineCircle = qmesh.vector.Circle(turbinePoint[0], turbinePoint[1], 10.0, 20, 'EPSG:32630')
                turbines.addFeature(turbineCircle.asQgsFeature())
            turbines.writeFile('turbines.shp')
            turbines.changeCoordRefSystem('EPSG:4326')
            turbineLoops = qmesh.vector.identifyLoops(turbines,
                      fixOpenLoops=True)
            turbinePolygons = qmesh.vector.identifyPolygons(turbineLoops,
                      meshedAreaPhysID = 3)
            #Reconstruct innerSound_plot_polygon object, so as to obtain
            # it back to EPSG:4326, recall we changed its CRS to EPSG:32630
            # While we could invoke the changeCoordRefSystem() method on it
            # we will not obtain the same point coordinates as before, leading
            # to problems later on.
            innerSound_plot_polygon = qmesh.vector.identifyPolygons(innerSound_plot_loops, 
                                                                             meshedAreaPhysID = 2)
            #Insert tidal plots and turbines into domain.
            domainLines, domainPolygons = \
               qmesh.vector.insertRegions(
                  loopShapes, polygonShapes,\
                  innerSound_plot_loops, innerSound_plot_polygon)
            domainLines, domainPolygons = \
               qmesh.vector.insertRegions(
                  domainLines, domainPolygons,\
                  turbineLoops, turbinePolygons)
            domainPolygons.writeFile('domainPolygons')
            #Create raster for mesh gradation towards full-resolution shorelines.
            GSHHS_fine_boundaries = qmesh.vector.Shapes()
            GSHHS_fine_boundaries.fromFile('GSHHS_f_L1_lines.shp')
            gradationRaster_shoreline = qmesh.raster.gradationToShapes()
            gradationRaster_shoreline.setShapes(GSHHS_fine_boundaries)
            gradationRaster_shoreline.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_shoreline.setRasterResolution(800,800)
            gradationRaster_shoreline.setGradationParameters(150.0,15000.0,0.5)
            gradationRaster_shoreline.calculateLinearGradation()
            gradationRaster_shoreline.writeNetCDF('gradationRaster_shoreline.nc')
            #Create raster for mesh gradation towards Shetlands shorelines.
            shetlands_shorelines = qmesh.vector.Shapes()
            shetlands_shorelines.fromFile('Shetlands_shoreline_gebco08.shp')
            gradationRaster_shetlands_shoreline = qmesh.raster.gradationToShapes()
            gradationRaster_shetlands_shoreline.setShapes(shetlands_shorelines)
            gradationRaster_shetlands_shoreline.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_shetlands_shoreline.setRasterResolution(800,800)
            gradationRaster_shetlands_shoreline.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_shetlands_shoreline.calculateLinearGradation()
            gradationRaster_shetlands_shoreline.writeNetCDF('gradation_to_Shetlands_shoreline.nc')
            #Create raster for mesh gradation towards 0m gebco contour on the Scottish mainland coast
            GEBCO08_0mContour = qmesh.vector.Shapes()
            GEBCO08_0mContour.fromFile('GEBCO08_0mContour.shp')
            gradationRaster_GEBCO08_0mContour = qmesh.raster.gradationToShapes()
            gradationRaster_GEBCO08_0mContour.setShapes(GEBCO08_0mContour)
            gradationRaster_GEBCO08_0mContour.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_GEBCO08_0mContour.setRasterResolution(800,800)
            gradationRaster_GEBCO08_0mContour.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_GEBCO08_0mContour.calculateLinearGradation()
            gradationRaster_GEBCO08_0mContour.writeNetCDF('gradationRaster_GEBCO08_0mContour.nc')
            #Create raster for mesh gradation towards GSHHS high-resolution lines on the Scottish mainland coast
            GSHHS_h_L1_lines = qmesh.vector.Shapes()
            GSHHS_h_L1_lines.fromFile('GSHHS_h_L1_lines.shp')
            gradationRaster_GSHHS_h_L1_lines = qmesh.raster.gradationToShapes()
            gradationRaster_GSHHS_h_L1_lines.setShapes(GSHHS_h_L1_lines)
            gradationRaster_GSHHS_h_L1_lines.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_GSHHS_h_L1_lines.setRasterResolution(800,800)
            gradationRaster_GSHHS_h_L1_lines.setGradationParameters(1500.0,15000.0,0.5)
            gradationRaster_GSHHS_h_L1_lines.calculateLinearGradation()
            gradationRaster_GSHHS_h_L1_lines.writeNetCDF('GSHHS_h_L1_lines_gradationRaster_.nc')
            #Create raster for mesh gradation towards Inner Sound
            gradationRaster_innerSound_plot = qmesh.raster.gradationToShapes()
            gradationRaster_innerSound_plot.setShapes(innerSound_plot_polygon)
            gradationRaster_innerSound_plot.setRasterBounds(-6.0, 1.0, 57.0, 62.0)
            gradationRaster_innerSound_plot.setRasterResolution(800,800)
            gradationRaster_innerSound_plot.setGradationParameters(10.0,15000.0,2.0,0.05)
            gradationRaster_innerSound_plot.calculateLinearGradation()
            gradationRaster_innerSound_plot.writeNetCDF('gradationRaster_innerSound_plot.nc')
            #Calculate overall mesh-metric raster
            meshMetricRaster = qmesh.raster.minimumRaster([gradationRaster_shoreline, \
                                                           gradationRaster_shetlands_shoreline, \
                                                           gradationRaster_GEBCO08_0mContour, \
                                                           gradationRaster_GSHHS_h_L1_lines, \
                                                           gradationRaster_innerSound_plot])
            meshMetricRaster.writeNetCDF('meshMetric.nc')
            #Create domain object and write gmsh files.
            domain = qmesh.mesh.Domain()
            domain.setGeometry(domainLines, domainPolygons)
            domain.setMeshMetricField(meshMetricRaster)
            domain.setTargetCoordRefSystem('PCC', fldFillValue=1000.0)
        except AssertionError:
            self.assert_(False)
        #Try meshing
        try:
            domain.gmsh(geoFilename='OrkneyShetlandIsles_PCC_InnerSoundTurbines.geo', \
                        fldFilename='OrkneyShetlandIsles_PCC_InnerSoundTurbines.fld', \
                        mshFilename='OrkneyShetlandIsles_PCC_InnerSoundTurbines.msh')
        except AssertionError:
            self.assert_(False)
        #Try converting into shapefile
        try:
            mesh = qmesh.mesh.Mesh()
            mesh.readGmsh('OrkneyShetlandIsles_PCC_InnerSoundTurbines.msh', 'PCC')
            mesh.writeShapefile('OrkneyShetlandIsles_PCC_InnerSoundTurbines')
        except AssertionError:
            self.assert_(False)
 
if __name__ == '__main__':
    unittest.main()
