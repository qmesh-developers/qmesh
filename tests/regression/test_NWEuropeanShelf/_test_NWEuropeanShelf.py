#!/usr/bin/env python

#    Copyright (C) 2013 Alexandros Avdis and others. See the AUTHORS file for a full list of copyright holders.
#
#    This file is part of QMesh.
#
#    QMesh is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    QMesh is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with QMesh.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import numpy as np
import qgis.core
import PyQt4.QtCore
import sys
import os
sys.path.append('../../../')
from qmesh.geometry.shapefileTools import lines2polygon
from qmesh.geometry.shapefileTools import shp2geo

line_filename = 'test_NWEuropeanShelf_lines.shp'
polygon_filename = 'test_NWEuropeanShelf_polygons.shp'
geo_filename = 'test_NWEuropeanShelf.geo'
msh_filename = 'test_NWEuropeanShelf.msh'

class TestNWEuropeanShelf(unittest.TestCase):
    '''Todo: add test documentation as class docstring '''

    def test_regressionTest(self):
        #Try initialising qgis API
        try:
            sys.stdout.write("\nInitialising QGIS:\n")
            qgis.core.QgsApplication.setPrefixPath('/usr', True)
            qgis.core.QgsApplication.initQgis()
            sys.stdout.write('    Found QGIS version '+
                                qgis.core.QGis.QGIS_VERSION+'\n')
            self.assert_(True)
        except AssertionError:
            self.assert_(False)
 
        #Try reading-in prepared boundaries (lines)
        try:
            sys.stdout.write("Reading lines from: "+line_filename+'\n')
            line_layer = qgis.core.QgsVectorLayer(line_filename,
                                                        'lines', 'ogr')
            self.assert_(line_layer.isValid())
        except AssertionError:
            self.assert_(False)
 
        #Try assembling the region (polygon) from lines
        try:
            sys.stdout.write("\nAssemblying regions:\n")
            sys.stdout.flush()
            outer_boundary_ids = ['1','2','3','4','5']
            outer_boundary_features = []
            inner_boundary_features = []
            features = line_layer.getFeatures()
            for feature in features:
                line_id = feature.attribute('id')
                if line_id in outer_boundary_ids:
                    outer_boundary_features.append(feature)
                else:
                    inner_boundary_features.append([feature])	
            #Open polygon file, set fields and CRS
            ID_field = qgis.core.QgsField('ID',PyQt4.QtCore.QVariant.Int)
            PhysID_field = qgis.core.QgsField('PhysID',
                                               PyQt4.QtCore.QVariant.Int)
            fields = qgis.core.QgsFields()
            fields.append(ID_field)
            fields.append(PhysID_field)
            coordinateReferenceSystem = line_layer.crs()
            polygonWriter = qgis.core.QgsVectorFileWriter(polygon_filename,
                    'CP1250', fields, qgis.core.QGis.WKBMultiPolygon,
                     coordinateReferenceSystem, 'ESRI Shapefile')
            #Assemble region (polygons) from lines and write to
            # polygon-shapefile
            polygonFeature = lines2polygon(outer_boundary_features,
                                       inner_boundary_features,3)
            polygonFeature.setFields(fields)
            polygonFeature.setAttribute('ID',1)
            polygonFeature.setAttribute('PhysID',1000)
            polygonWriter.addFeature(polygonFeature)
            del polygonWriter
            self.assert_(True)
        except AssertionError:
            self.assert_(False)

        #Convert to Gmsh geometry
        try:
            sys.stdout.write("\nConverting to Gmsh geometry:\n")
            shp2geo(line_filename, polygon_filename, geo_filename,
                  coordinateReferenceSystem, 'EPSG:32630', 0.0, 1)
            self.assert_(True)
        except AssertionError:
            self.assert_(False)

        #Meshing with Gmsh
        try:
            sys.stdout.write("\nMeshing with Gmsh:\n")
            os.system('gmsh -2 '+geo_filename)
        except AssertionError:
            self.assert_(False)

        #Cleaning up
        #try:
        #    sys.stdout.write("\nCleaning up:\n")
        #    os.system('rm -rfv *.geo *.msh'+
        #                 ' test_NWEuropeanShelf_polygons.*')
        #except AssertionError:
        #    self.assert_(False)

if __name__ == '__main__':
    unittest.main()
