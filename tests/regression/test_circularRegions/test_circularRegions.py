#!/usr/bin/env python

#    Copyright (C) 2013 Alexandros Avdis and others. See the AUTHORS file for a full list of copyright holders.
#
#    This file is part of QMesh.
#
#    QMesh is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    QMesh is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with QMesh.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import numpy as np
import qgis.core
import PyQt4.QtCore
# so we import local python before any other
#import sys
#sys.path.insert(0,"../../../")
import qmesh

class TestCircularRegions(unittest.TestCase):
    '''Test region insertion in qmesh.'''

    def setUp(self):
        qmesh.LOG.setLevel('WARNING')
        qmesh.initialise()
        import os
        self.thisPath = os.path.dirname(os.path.realpath(__file__))
        self.circularDomain_line_filename = self.thisPath+'/test_outerCircle_lines.shp'
        self.circularDomain_polygon_filename = self.thisPath+'/test_outerCircle_polygons.shp'
        self.circularRegion30_line_filename = self.thisPath+'/test_circularRegion30_lines.shp'
        self.circularRegion30_polygon_filename = \
                                          self.thisPath+'/test_circularRegion30_polygons.shp'
        self.circularRegions5_line_filename = \
                                  self.thisPath+'/test_circularRegions_5degRadius_lines.shp'
        self.circularRegions5_polygon_filename = \
                              self.thisPath+'/test_circularRegions_5degRadius_polygons.shp'

        #Try initialising qgis API
        try:
            qgis.core.QgsApplication.setPrefixPath('/usr', True)
            qgis.core.QgsApplication.initQgis()
            self.assert_(True)
        except AssertionError:
            self.assert_(False)

    def create_outerRegion_shapefiles(self):
        '''Test shape creation with qgis API. Create a circle, in EPSG:4326, centered at (0,0) and radius of 80. Write to files as line and polygon.'''
        #Try creating fields
        try:
            IDfield = qgis.core.QgsField("ID", 
                                    PyQt4.QtCore.QVariant.Int)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #descriptionField = qgis.core.QgsField("Descript", 
            #                        PyQt4.QtCore.QVariant.String)
            physicalIDfield = qgis.core.QgsField("PhysID", 
                                    PyQt4.QtCore.QVariant.Int)
            fields = qgis.core.QgsFields()
            fields.append(IDfield)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #fields.append(descriptionField)
            fields.append(physicalIDfield)
            self.assert_(True)
        except AssertionError:
            self.assert_(False)

        #Try creating Coordinate Reference System
        try:
            coordinateReferenceSystem = \
                                  qgis.core.QgsCoordinateReferenceSystem()
            coordinateReferenceSystem.createFromString("EPSG:4326")
            self.assert_(coordinateReferenceSystem.isValid())
        except AssertionError:
            self.assert_(False)

        #Try creating shapefile-writer for line
        try:
            lineWriter = qgis.core.QgsVectorFileWriter(
                         self.circularDomain_line_filename,
                         "CP1250", fields, qgis.core.QGis.WKBLineString,
                         coordinateReferenceSystem, "ESRI Shapefile")
            self.assert_(
                 lineWriter.hasError() == \
                                   qgis.core.QgsVectorFileWriter.NoError)
        except AssertionError:
            self.assert_(False)

        #Try creating line feature.
        try:
            #Calculate point coordinates
            numberPoints=1000
            deltaAngle=360./numberPoints
            centerPointCoords = [0.,0.]
            circleRadius = 80.0
            pointsCoords = []
            pointsCoords.append([centerPointCoords[0] + \
                          circleRadius, centerPointCoords[1]])
            for index in np.arange(1,numberPoints):
                longitude = centerPointCoords[0] + \
                          circleRadius*np.cos(np.radians(index*deltaAngle))
                latitude = centerPointCoords[1] + \
                          circleRadius*np.sin(np.radians(index*deltaAngle))
                pointsCoords.append([longitude, latitude])
            pointsCoords.append([centerPointCoords[0] + \
                          circleRadius, centerPointCoords[1]])
            #Populate QGIS feature
            line_qgis=[]
            for point in pointsCoords:
                line_qgis.append(qgis.core.QgsPoint(point[0],point[1]))
            feature1 = qgis.core.QgsFeature()
            feature1.setGeometry(
                            qgis.core.QgsGeometry.fromPolyline(line_qgis))
            feature1.setFields(fields)
            feature1.setAttribute('ID',1)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #feature1.setAttribute('Descript','Outer boundary')
            feature1.setAttribute('PhysID',1000)
            lineWriter.addFeature(feature1)
            self.assert_(
                 lineWriter.hasError() == \
                              qgis.core.QgsVectorFileWriter.NoError)
            del lineWriter
        except AssertionError:
            self.assert_(False)

        #Try creating region from the line
        try:
            #Open polygon file
            polygonWriter = qgis.core.QgsVectorFileWriter(
                     self.circularDomain_polygon_filename, "CP1250", 
                     fields, qgis.core.QGis.WKBMultiPolygon,
                     coordinateReferenceSystem, "ESRI Shapefile")
            #Create polygon feature
            polygonFeature1 = qmesh.vector.lines2polygon( [feature1], [])
            polygonFeature1.setFields(fields)
            polygonFeature1.setAttribute('ID',13)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #polygonFeature1.setAttribute('Descript','Circular Domain')
            polygonFeature1.setAttribute('PhysID',10000)
            polygonWriter.addFeature(polygonFeature1)
            self.assert_(
                 polygonWriter.hasError() == \
                              qgis.core.QgsVectorFileWriter.NoError)
            del polygonWriter
        except AssertionError:
            self.assert_(False)

    def create_circularRegion30_shapefiles(self):
        '''Test shape creation with qgis API. Create a circle, in EPSG:4326, centered at (0,10) and radius of 30. Write to files as line and polygon.'''
        #Try creating fields
        try:
            IDfield = qgis.core.QgsField("ID", PyQt4.QtCore.QVariant.Int)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #descriptionField = qgis.core.QgsField("Descript", \
            #                               PyQt4.QtCore.QVariant.String)
            physicalIDfield = qgis.core.QgsField("PhysID", \
                                           PyQt4.QtCore.QVariant.Int)
            fields = qgis.core.QgsFields()
            fields.append(IDfield)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #fields.append(descriptionField)
            fields.append(physicalIDfield)
            self.assert_(True)
        except AssertionError:
            self.assert_(False)

        #Try creating Coordinate Reference System
        try:
            coordinateReferenceSystem = \
                                  qgis.core.QgsCoordinateReferenceSystem()
            coordinateReferenceSystem.createFromString("EPSG:4326")
            self.assert_(coordinateReferenceSystem.isValid())
        except AssertionError:
            self.assert_(False)

        #Try creating shapefile-writer for line
        try:
            lineWriter = qgis.core.QgsVectorFileWriter(
                         self.circularRegion30_line_filename,
                         "CP1250", fields, qgis.core.QGis.WKBLineString,
                         coordinateReferenceSystem, "ESRI Shapefile")
            self.assert_(
                 lineWriter.hasError() == \
                 qgis.core.QgsVectorFileWriter.NoError)
        except AssertionError:
            self.assert_(False)

        #Try greating line feature.
        try:
            #Calculate point coordinates
            numberPoints=1000
            deltaAngle=360./numberPoints
            centerPointCoords = [0.,10.]
            circleRadius = 30.0
            pointsCoords = []
            pointsCoords.append([centerPointCoords[0] + \
                          circleRadius, centerPointCoords[1]])
            for index in np.arange(1,numberPoints):
                longitude = centerPointCoords[0] + \
                          circleRadius*np.cos(np.radians(index*deltaAngle))
                latitude = centerPointCoords[1] + \
                          circleRadius*np.sin(np.radians(index*deltaAngle))
                pointsCoords.append([longitude, latitude])
            pointsCoords.append([centerPointCoords[0] + \
                          circleRadius, centerPointCoords[1]])
            #Populate QGIS feature
            line_qgis=[]
            for point in pointsCoords:
                line_qgis.append(qgis.core.QgsPoint(point[0],point[1]))
            feature2 = qgis.core.QgsFeature()
            feature2.setGeometry(
                 qgis.core.QgsGeometry.fromPolyline(line_qgis))
            feature2.setFields(fields)
            feature2.setAttribute('ID',10)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #feature2.setAttribute('Descript','30-degree radius circle')
            lineWriter.addFeature(feature2)
            self.assert_(
                 lineWriter.hasError() == \
                 qgis.core.QgsVectorFileWriter.NoError)
            del lineWriter
        except AssertionError:
            self.assert_(False)

        #Try creating region from the line.
        try:
            #Open polygon file
            polygonWriter = qgis.core.QgsVectorFileWriter(
                     self.circularRegion30_polygon_filename, "CP1250",
                     fields, qgis.core.QGis.WKBMultiPolygon,
                     coordinateReferenceSystem, "ESRI Shapefile")
            #Create polygon feature
            polygonFeature2 = qmesh.vector.lines2polygon([feature2], [])
            polygonFeature2.setFields(fields)
            polygonFeature2.setAttribute('ID',13)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #polygonFeature2.setAttribute('Descript',\
            #     'Region marked-out by 30-degree-radius circle in lon-lat.')
            polygonFeature2.setAttribute('PhysID',20000)
            polygonWriter.addFeature(polygonFeature2)
            self.assert_(
                 polygonWriter.hasError() == \
                 qgis.core.QgsVectorFileWriter.NoError)
            del polygonWriter
        except AssertionError:
            self.assert_(False)

    def create_circularRegions5_shapefiles(self):
        '''Test shape creation with qgis API. Create a circle, in EPSG:4326, centered at (20,0) and radius of 5. Write to files as line and polygon.'''
        #Try creating fields
        try:
            IDfield = qgis.core.QgsField("ID", PyQt4.QtCore.QVariant.Int)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #descriptionField = qgis.core.QgsField("Descript", \
            #                               PyQt4.QtCore.QVariant.String)
            physicalIDfield = qgis.core.QgsField("PhysID", \
                                           PyQt4.QtCore.QVariant.Int)
            fields = qgis.core.QgsFields()
            fields.append(IDfield)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #fields.append(descriptionField)
            fields.append(physicalIDfield)
            self.assert_(True)
        except AssertionError:
            self.assert_(False)

        #Try creating Coordinate Reference System
        try:
            coordinateReferenceSystem = \
                                  qgis.core.QgsCoordinateReferenceSystem()
            coordinateReferenceSystem.createFromString("EPSG:4326")
            self.assert_(coordinateReferenceSystem.isValid())
        except AssertionError:
            self.assert_(False)

        #Try creating shapefile-writer for line
        try:
            lineWriter = qgis.core.QgsVectorFileWriter(
                         self.circularRegions5_line_filename,
                         "CP1250", fields, qgis.core.QGis.WKBLineString,
                         coordinateReferenceSystem, "ESRI Shapefile")
            self.assert_(
                 lineWriter.hasError() == \
                 qgis.core.QgsVectorFileWriter.NoError)
        except AssertionError:
            self.assert_(False)

        #Try greating line feature.
        try:
            #Calculate point coordinates for first region in the set.
            numberPoints=1000
            deltaAngle=360./numberPoints
            centerPointCoords = [20.,0.]
            circleRadius = 5.0
            pointsCoords = []
            pointsCoords.append([centerPointCoords[0] + \
                          circleRadius, centerPointCoords[1]])
            for index in np.arange(1,numberPoints):
                longitude = centerPointCoords[0] + \
                          circleRadius*np.cos(np.radians(index*deltaAngle))
                latitude = centerPointCoords[1] + \
                          circleRadius*np.sin(np.radians(index*deltaAngle))
                pointsCoords.append([longitude, latitude])
            pointsCoords.append([centerPointCoords[0] + \
                          circleRadius, centerPointCoords[1]])
            #Populate QGIS feature
            line_qgis=[]
            for point in pointsCoords:
                line_qgis.append(qgis.core.QgsPoint(point[0],point[1]))
            feature1 = qgis.core.QgsFeature()
            feature1.setGeometry(
                 qgis.core.QgsGeometry.fromPolyline(line_qgis))
            feature1.setFields(fields)
            feature1.setAttribute('ID',10)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #feature1.setAttribute('Descript','5-degree radius circle')
            lineWriter.addFeature(feature1)
            self.assert_(
                 lineWriter.hasError() == \
                 qgis.core.QgsVectorFileWriter.NoError)
            #Calculate point coordinates for second region in the set.
            numberPoints=1000
            deltaAngle=360./numberPoints
            centerPointCoords = [0.,10.]
            circleRadius = 5.0
            pointsCoords = []
            pointsCoords.append([centerPointCoords[0] + \
                          circleRadius, centerPointCoords[1]])
            for index in np.arange(1,numberPoints):
                longitude = centerPointCoords[0] + \
                          circleRadius*np.cos(np.radians(index*deltaAngle))
                latitude = centerPointCoords[1] + \
                          circleRadius*np.sin(np.radians(index*deltaAngle))
                pointsCoords.append([longitude, latitude])
            pointsCoords.append([centerPointCoords[0] + \
                          circleRadius, centerPointCoords[1]])
            #Populate QGIS feature
            line_qgis=[]
            for point in pointsCoords:
                line_qgis.append(qgis.core.QgsPoint(point[0],point[1]))
            feature2 = qgis.core.QgsFeature()
            feature2.setGeometry(
                 qgis.core.QgsGeometry.fromPolyline(line_qgis))
            feature2.setFields(fields)
            feature2.setAttribute('ID',11)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #feature2.setAttribute('Descript','5-degree radius circle')
            lineWriter.addFeature(feature2)
            self.assert_(
                 lineWriter.hasError() == \
                 qgis.core.QgsVectorFileWriter.NoError)

            del lineWriter
        except AssertionError:
            self.assert_(False)

        #Try creating regions from the lines.
        try:
            #Open polygon file
            polygonWriter = qgis.core.QgsVectorFileWriter(
                     self.circularRegions5_polygon_filename,
                     "CP1250", fields, qgis.core.QGis.WKBMultiPolygon,
                     coordinateReferenceSystem, "ESRI Shapefile")
            #Create polygon feature for first region in the lot
            polygonFeature1 = qmesh.vector.lines2polygon([feature1], [])
            polygonFeature1.setFields(fields)
            polygonFeature1.setAttribute('ID',20)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #polygonFeature1.setAttribute('Descript',\
            #     'Region marked-out by 5-degree-radius circle in lon-lat.')
            polygonFeature1.setAttribute('PhysID',80000)
            polygonWriter.addFeature(polygonFeature1)
            self.assert_(
                 polygonWriter.hasError() == \
                 qgis.core.QgsVectorFileWriter.NoError)
            #Create polygon feature for second region in the lot
            polygonFeature2 = qmesh.vector.lines2polygon([feature2], [])
            polygonFeature2.setFields(fields)
            polygonFeature2.setAttribute('ID',21)
            #Old GDAL versions seem to throw a warning, keeping off for now
            #polygonFeature2.setAttribute('Descript',\
            #     'Region marked-out by 5-degree-radius circle in lon-lat.')
            polygonFeature2.setAttribute('PhysID',80000)
            polygonWriter.addFeature(polygonFeature2)
            self.assert_(
                 polygonWriter.hasError() == \
                 qgis.core.QgsVectorFileWriter.NoError)
            del polygonWriter
        except AssertionError:
            self.assert_(False)

    def test_singleRegion_PCC_noMeshMetric(self):
        '''Test reading-in lines and polygons from file and generating a mesh, in planet-centered-cartesian.

        Rationale: This function is an incremental step towards testing
        "region insertion" functionality in qmesh. However, no regions are
        created nor inserted in this test. Only the "outer" domain is created
        and meshed. The input domain is in EPSG:4326 and the output mesh is
        in 'Planet-Centered-Cartesian'. The domain is a circle centered at
        (0.0, 0.0)degrees, with an 80 degree radius.'''
        function_name = self.test_singleRegion_PCC_noMeshMetric.__name__
        #Create shapefiles.
        self.create_outerRegion_shapefiles()
        #Read-in shapefiles
        try:
            outerRegionLines = qmesh.vector.Shapes()
            outerRegionLines.fromFile(self.circularDomain_line_filename)
            outerRegionPolygons = qmesh.vector.shapefileTools.Shapes()
            outerRegionPolygons.fromFile(self.circularDomain_polygon_filename)
        except AssertionError:
            self.assert_(False)
        #Create Gmsh domain object
        try:
            domain = qmesh.mesh.Domain()
            domain.setGeometry(outerRegionLines, outerRegionPolygons)
            domain.setTargetCoordRefSystem('PCC')
        except AssertionError:
            self.assert_(False)
        #Meshing with Gmsh
        try:
            domain.gmsh(geoFilename=self.thisPath+'/'+function_name+'.geo',
                        mshFilename=self.thisPath+'/'+function_name+'.msh',
                        gmshAlgo = 'front2d')
        except AssertionError:
            self.assert_(False)

    def test_singleRegion_UTM30_noMeshMetric(self):
        '''Test reading-in lines and polygons from file and generating a mesh, in UTM30.

        Rationale: This function is an incremental step towards testing
        "region insertion" functionality in qmesh. However, no regions are
        created nor inserted in this test, only the "outer" domain is created
        and meshed. The input domain is in EPSG:4326 and the output mesh is
        in EPSG:32630 - UTM30. The domain is a circle centered at (0.0, 0.0)degrees,
        with an 80 degree radius.'''
        function_name = self.test_singleRegion_UTM30_noMeshMetric.__name__
        #Create shapefiles.
        self.create_outerRegion_shapefiles()
        #Read-in shapefiles
        try:
            outerRegionLines = qmesh.vector.Shapes()
            outerRegionLines.fromFile(self.circularDomain_line_filename)
            outerRegionPolygons = qmesh.vector.Shapes()
            outerRegionPolygons.fromFile(self.circularDomain_polygon_filename)
        except AssertionError:
            self.assert_(False)
        #Create Gmsh domain object
        try:
            domain = qmesh.mesh.Domain()
            domain.setGeometry(outerRegionLines, outerRegionPolygons)
            domain.setTargetCoordRefSystem('EPSG:32630')
        except AssertionError:
            self.assert_(False)
        #Meshing with Gmsh
        try:
            domain.gmsh(geoFilename=self.thisPath+'/'+function_name+'.geo',
                        mshFilename=self.thisPath+'/'+function_name+'.msh',
                        gmshAlgo = 'front2d')
        except AssertionError:
            self.assert_(False)

    def test_singleRegion_UTM31_noMeshMetric(self):
        '''Test reading-in lines and polygons from file and generating a mesh, in UTM31.

        Rationale: This function is an incremental step towards testing 
        "region insertion" functionality in qmesh. However no regions are
        created nor inserted in this test, only the "outer" domain is created
        and meshed. The input domain is in EPSG:4326 and the output mesh is
        in EPSG:32631 - UTM31. The  domain is a circle centered at (0.0, 0.0)degrees,
        with an 80 degree radius.'''
        function_name = self.test_singleRegion_UTM31_noMeshMetric.__name__
        #Create shapefiles.
        self.create_outerRegion_shapefiles()
        #Read-in shapefiles
        try:
            outerRegionLines = qmesh.vector.Shapes()
            outerRegionLines.fromFile(self.circularDomain_line_filename)
            outerRegionPolygons = qmesh.vector.Shapes()
            outerRegionPolygons.fromFile(self.circularDomain_polygon_filename)
        except AssertionError:
            self.assert_(False)
        #Create Gmsh domain object
        try:
            domain = qmesh.mesh.Domain()
            domain.setGeometry(outerRegionLines, outerRegionPolygons)
            domain.setTargetCoordRefSystem('EPSG:32631')
        except AssertionError:
            self.assert_(False)
        #Meshing with Gmsh
        try:
            domain.gmsh(geoFilename=self.thisPath+'/'+function_name+'.geo',
                        mshFilename=self.thisPath+'/'+function_name+'.msh',
                        gmshAlgo = 'front2d')
        except AssertionError:
            self.assert_(False)

    def test_twoRegion_PCC_noMeshMetric(self):
        '''Test multiple region meshing and labelling. Domain is composed of two regions, and a mesh is generated in Planet-Centered-Cartesian. No mesh gradation is specified.'''
        function_name = self.test_twoRegion_PCC_noMeshMetric.__name__
        #Create shapefiles.
        try:
            self.create_outerRegion_shapefiles()
            self.create_circularRegion30_shapefiles()
        except AssertionError:
            self.assert_(False)
        #Read-in shapefiles
        try:
            outerRegionLines = qmesh.vector.Shapes()
            outerRegionLines.fromFile(self.circularDomain_line_filename)
            outerRegionPolygons = qmesh.vector.Shapes()
            outerRegionPolygons.fromFile(self.circularDomain_polygon_filename)
            circularRegion30Lines = qmesh.vector.Shapes()
            circularRegion30Lines.fromFile(self.circularRegion30_line_filename)
            circularRegion30Polygons = qmesh.vector.Shapes()
            circularRegion30Polygons.fromFile(self.circularRegion30_polygon_filename)
        except AssertionError:
            self.assert_(False)
        #Insert smaller region into larger.
        try:
            domainLines, domainPolygons = \
              qmesh.vector.insertRegions(
                outerRegionLines,\
                outerRegionPolygons,\
                circularRegion30Lines,\
                circularRegion30Polygons)
        except AssertionError:
            self.assert_(False)
        #Create Gmsh domain object
        try:
            domain = qmesh.mesh.Domain()
            domain.setGeometry(domainLines, domainPolygons)
            domain.setTargetCoordRefSystem('PCC')
        except AssertionError:
            self.assert_(False)
        #Meshing with Gmsh
        try:
            domain.gmsh(geoFilename=self.thisPath+'/'+function_name+'.geo',
                        mshFilename=self.thisPath+'/'+function_name+'.msh',
                        gmshAlgo = 'front2d')
        except AssertionError:
            self.assert_(False)

    def test_twoRegion_UTM30_noMeshMetric(self):
        '''Test multiple region meshing and labelling. Domain is composed of two regions, and a mesh is generated in UTM30. No mesh gradation is specified.'''

        function_name = self.test_twoRegion_UTM30_noMeshMetric.__name__
        #Create shapefiles.
        try:
            self.create_outerRegion_shapefiles()
            self.create_circularRegion30_shapefiles()
        except AssertionError:
            self.assert_(False)
        #Read-in shapefiles
        try:
            outerRegionLines = qmesh.vector.Shapes()
            outerRegionLines.fromFile(self.circularDomain_line_filename)
            outerRegionPolygons = qmesh.vector.Shapes()
            outerRegionPolygons.fromFile(self.circularDomain_polygon_filename)
            circularRegion30Lines = qmesh.vector.Shapes()
            circularRegion30Lines.fromFile(self.circularRegion30_line_filename)
            circularRegion30Polygons = qmesh.vector.Shapes()
            circularRegion30Polygons.fromFile(self.circularRegion30_polygon_filename)
        except AssertionError:
            self.assert_(False)
        #Insert smaller region into larger.
        try:
            domainLines, domainPolygons = \
              qmesh.vector.insertRegions(
                outerRegionLines,\
                outerRegionPolygons,\
                circularRegion30Lines,\
                circularRegion30Polygons)
        except AssertionError:
            self.assert_(False)
        #Create Gmsh domain object
        try:
            domain = qmesh.mesh.Domain()
            domain.setGeometry(domainLines, domainPolygons)
            domain.setTargetCoordRefSystem('EPSG:32630')
        except AssertionError:
            self.assert_(False)
        #Meshing with Gmsh
        try:
            domain.gmsh(geoFilename=self.thisPath+'/'+function_name+'.geo',
                        mshFilename=self.thisPath+'/'+function_name+'.msh',
                        gmshAlgo = 'front2d')
        except AssertionError:
            self.assert_(False)

    def test_twoRegion_PCC_gradatedMesh(self):
        '''Test multiple region meshing and labelling. Domain is composed of two regions, and a mesh is generated in Planet-Centered-Cartesian. Mesh gradates towards inner region.'''
        function_name = self.test_twoRegion_PCC_gradatedMesh.__name__
        #Create shapefiles
        try:
            self.create_outerRegion_shapefiles()
            self.create_circularRegion30_shapefiles()
        except AssertionError:
            self.assert_(False)
        #Read-in shapefiles
        try:
            outerRegionLines = qmesh.vector.Shapes()
            outerRegionLines.fromFile(self.circularDomain_line_filename)
            outerRegionPolygons = qmesh.vector.Shapes()
            outerRegionPolygons.fromFile(self.circularDomain_polygon_filename)
            circularRegion30Lines = qmesh.vector.Shapes()
            circularRegion30Lines.fromFile(self.circularRegion30_line_filename)
            circularRegion30Polygons = qmesh.vector.Shapes()
            circularRegion30Polygons.fromFile(self.circularRegion30_polygon_filename)
        except AssertionError:
            self.assert_(False)
        #Insert smaller region into larger.
        try:
            domainLines, domainPolygons = \
                qmesh.vector.insertRegions(
                   outerRegionLines, outerRegionPolygons,\
                   circularRegion30Lines, circularRegion30Polygons)
        except AssertionError:
            self.assert_(False)
        #Create gradated mesh metric raster for inserted region.
        try:
            raster = qmesh.raster.gradationToShapes()
            raster.setShapes(circularRegion30Polygons)
            raster.setRasterBounds(-85.0,85.0,-85.0,85.0)
            raster.setRasterResolution(100,100)
            raster.setGradationParameters(100000.0,500000.0,20.0)
            raster.calculateLinearGradation()
        except AssertionError:
            self.assert_(False)
        #Create Gmsh domain object
        try:
            domain = qmesh.mesh.Domain()
            domain.setGeometry(domainLines, domainPolygons)
            domain.setMeshMetricField(raster)
            domain.setTargetCoordRefSystem('PCC')
        except AssertionError:
            self.assert_(False)
        #Meshing with Gmsh
        try:
            domain.gmsh(geoFilename=self.thisPath+'/'+function_name+'.geo',
                        fldFilename=self.thisPath+'/'+function_name+'.fld',
                        mshFilename=self.thisPath+'/'+function_name+'.msh',
                        gmshAlgo = 'front2d')
        except AssertionError:
            self.assert_(False)

    def test_twoRegion_UTM30_gradatedMesh(self):
        '''Test multiple region meshing and labelling. Domain is composed of two regions, and a mesh is generated in UTM30. Mesh gradates towards inner region.'''
        function_name = self.test_twoRegion_UTM30_gradatedMesh.__name__
        #Create shapefiles
        try:
            self.create_outerRegion_shapefiles()
            self.create_circularRegion30_shapefiles()
        except AssertionError:
            self.assert_(False)
        #Read-in shapefiles
        try:
            outerRegionLines = qmesh.vector.Shapes()
            outerRegionLines.fromFile(self.circularDomain_line_filename)
            outerRegionPolygons = qmesh.vector.Shapes()
            outerRegionPolygons.fromFile(self.circularDomain_polygon_filename)
            circularRegion30Lines = qmesh.vector.Shapes()
            circularRegion30Lines.fromFile(self.circularRegion30_line_filename)
            circularRegion30Polygons = qmesh.vector.Shapes()
            circularRegion30Polygons.fromFile(self.circularRegion30_polygon_filename)
        except AssertionError:
            self.assert_(False)
        #Insert smaller region into larger.
        try:
            domainLines, domainPolygons = \
                qmesh.vector.insertRegions(
                   outerRegionLines, outerRegionPolygons,\
                   circularRegion30Lines, circularRegion30Polygons)
        except AssertionError:
            self.assert_(False)
        #Create gradated mesh metric raster for inserted region.
        try:
            raster = qmesh.raster.gradationToShapes()
            raster.setShapes(circularRegion30Polygons)
            raster.setRasterBounds(-85.0,85.0,-85.0,85.0)
            raster.setRasterResolution(100,100)
            raster.setGradationParameters(100000.0,500000.0,20.0)
            raster.calculateLinearGradation()
        except AssertionError:
            self.assert_(False)
        #Create Gmsh domain object
        try:
            domain = qmesh.mesh.Domain()
            domain.setGeometry(domainLines, domainPolygons)
            domain.setMeshMetricField(raster)
            domain.setTargetCoordRefSystem('EPSG:32630', fldFillValue=1000.0)
        except AssertionError:
            self.assert_(False)
        #Meshing with Gmsh
        try:
            domain.gmsh(geoFilename=self.thisPath+'/'+function_name+'.geo',
                        fldFilename=self.thisPath+'/'+function_name+'.fld',
                        mshFilename=self.thisPath+'/'+function_name+'.msh',
                        gmshAlgo = 'front2d')
        except AssertionError:
            self.assert_(False)

    def test_twoRegion_UTM31_gradatedMesh(self):
        '''Test multiple region meshing and labelling. Domain is composed of two regions, and a mesh is generated in UTM31. Mesh gradates towards inner region.'''
        function_name = self.test_twoRegion_UTM31_gradatedMesh.__name__
        #Create shapefiles
        try:
            self.create_outerRegion_shapefiles()
            self.create_circularRegion30_shapefiles()
        except AssertionError:
            self.assert_(False)
        #Read-in shapefiles
        try:
            outerRegionLines = qmesh.vector.Shapes()
            outerRegionLines.fromFile(self.circularDomain_line_filename)
            outerRegionPolygons = qmesh.vector.Shapes()
            outerRegionPolygons.fromFile(self.circularDomain_polygon_filename)
            circularRegion30Lines = qmesh.vector.Shapes()
            circularRegion30Lines.fromFile(self.circularRegion30_line_filename)
            circularRegion30Polygons = qmesh.vector.Shapes()
            circularRegion30Polygons.fromFile(self.circularRegion30_polygon_filename)
        except AssertionError:
            self.assert_(False)
        #Insert smaller region into larger.
        try:
            domainLines, domainPolygons = \
                qmesh.vector.insertRegions(
                   outerRegionLines, outerRegionPolygons,\
                   circularRegion30Lines, circularRegion30Polygons)
        except AssertionError:
            self.assert_(False)
        #Create gradated mesh metric raster for inserted region.
        try:
            raster = qmesh.raster.gradationToShapes()
            raster.setShapes(circularRegion30Polygons)
            raster.setRasterBounds(-85.0,85.0,-85.0,85.0)
            raster.setRasterResolution(100,100)
            raster.setGradationParameters(100000.0,500000.0,20.0)
            raster.calculateLinearGradation()
        except AssertionError:
            self.assert_(False)
        #Create Gmsh domain object
        try:
            domain = qmesh.mesh.Domain()
            domain.setGeometry(domainLines, domainPolygons)
            domain.setMeshMetricField(raster)
            domain.setTargetCoordRefSystem('EPSG:32631', fldFillValue=1000.0)
        except AssertionError:
            self.assert_(False)
        #Meshing with Gmsh
        try:
            domain.gmsh(geoFilename=self.thisPath+'/'+function_name+'.geo',
                        fldFilename=self.thisPath+'/'+function_name+'.fld',
                        mshFilename=self.thisPath+'/'+function_name+'.msh',
                        gmshAlgo = 'front2d')
        except AssertionError:
            self.assert_(False)

    def test_multipleRegion_PCC(self):
        '''Test multiple region meshing and labelling. Domain is composed of four regions, and a mesh is generated in Planet-Centered-Cartesian.'''
        function_name = self.test_multipleRegion_PCC.__name__
        #Create shapefiles
        try:
            self.create_outerRegion_shapefiles()
            self.create_circularRegion30_shapefiles()
            self.create_circularRegions5_shapefiles()
        except AssertionError:
            self.assert_(False)
        #Read-in shapefiles
        try:
            outerRegionLines = qmesh.vector.Shapes()
            outerRegionLines.fromFile(self.circularDomain_line_filename)
            outerRegionPolygons = qmesh.vector.Shapes()
            outerRegionPolygons.fromFile(self.circularDomain_polygon_filename)
            circularRegion30Lines = qmesh.vector.Shapes()
            circularRegion30Lines.fromFile(self.circularRegion30_line_filename)
            circularRegion30Polygons = qmesh.vector.Shapes()
            circularRegion30Polygons.fromFile(self.circularRegion30_polygon_filename)
            circularRegion5Lines = qmesh.vector.Shapes()
            circularRegion5Lines.fromFile(self.circularRegions5_line_filename)
            circularRegion5Polygons = qmesh.vector.Shapes()
            circularRegion5Polygons.fromFile(self.circularRegions5_polygon_filename)
        except AssertionError:
            self.assert_(False)
        #Insert smaller region into larger.
        try:
            domainLines, domainPolygons = \
               qmesh.vector.shapefileTools.insertRegions(
                  outerRegionLines, outerRegionPolygons,\
                  circularRegion30Lines, circularRegion30Polygons)
            domainLines, domainPolygons = \
               qmesh.vector.shapefileTools.insertRegions(
                  domainLines, domainPolygons,\
                  circularRegion5Lines, circularRegion5Polygons)
        except AssertionError:
            self.assert_(False)
        #Create gradated mesh metric raster for inserted region.
        try:
            rasterI = qmesh.raster.gradationToShapes()
            rasterI.setShapes(circularRegion5Polygons)
            rasterI.setRasterBounds(-85.0,85.0,-85.0,85.0)
            rasterI.setRasterResolution(200,200)
            rasterI.setGradationParameters(10000.0,500000.0,30.0)
            rasterI.calculateLinearGradation()
            rasterII = qmesh.raster.gradationToShapes()
            rasterII.setShapes(circularRegion30Polygons)
            rasterII.setRasterBounds(-85.0,85.0,-85.0,85.0)
            rasterII.setRasterResolution(200,200)
            rasterII.setGradationParameters(100000.0,500000.0,20.0)
            rasterII.calculateLinearGradation()
            rasterIII = qmesh.raster.minimumRaster([rasterI, rasterII])
        except AssertionError:
            self.assert_(False)
        #Create domain object
        try:
            domain = qmesh.mesh.Domain()
            domain.setGeometry(domainLines, domainPolygons)
            domain.setMeshMetricField(rasterIII)
            domain.setTargetCoordRefSystem('PCC', fldFillValue=0.0)
        except AssertionError:
            self.assert_(False)
        #Meshing with Gmsh
        try:
            domain.gmsh(geoFilename=self.thisPath+'/'+function_name+'.geo',
                        fldFilename=self.thisPath+'/'+function_name+'.fld',
                        mshFilename=self.thisPath+'/'+function_name+'.msh',
                        gmshAlgo = 'front2d')
        except AssertionError:
            self.assert_(False)

    def test_multipleRegion_UTM30(self):
        '''Test multiple region meshing and labelling. Domain is composed of four regions, and a mesh is generated in UTM30.'''
        function_name = self.test_multipleRegion_UTM30.__name__
        #Create shapefiles
        try:
            self.create_outerRegion_shapefiles()
            self.create_circularRegion30_shapefiles()
            self.create_circularRegions5_shapefiles()
        except AssertionError:
            self.assert_(False)
        #Read-in shapefiles
        try:
            outerRegionLines = qmesh.vector.Shapes()
            outerRegionLines.fromFile(self.circularDomain_line_filename)
            outerRegionPolygons = qmesh.vector.Shapes()
            outerRegionPolygons.fromFile(self.circularDomain_polygon_filename)
            circularRegion30Lines = qmesh.vector.Shapes()
            circularRegion30Lines.fromFile(self.circularRegion30_line_filename)
            circularRegion30Polygons = qmesh.vector.Shapes()
            circularRegion30Polygons.fromFile(self.circularRegion30_polygon_filename)
            circularRegion5Lines = qmesh.vector.Shapes()
            circularRegion5Lines.fromFile(self.circularRegions5_line_filename)
            circularRegion5Polygons = qmesh.vector.Shapes()
            circularRegion5Polygons.fromFile(self.circularRegions5_polygon_filename)
        except AssertionError:
            self.assert_(False)
        #Insert smaller region into larger.
        try:
            domainLines, domainPolygons = \
               qmesh.vector.insertRegions(
                  outerRegionLines, outerRegionPolygons,\
                  circularRegion30Lines, circularRegion30Polygons)
            domainLines, domainPolygons = \
               qmesh.vector.insertRegions(
                  domainLines, domainPolygons,\
                  circularRegion5Lines, circularRegion5Polygons)
        except AssertionError:
            self.assert_(False)
        #Create gradated mesh metric raster for inserted region.
        try:
            rasterI = qmesh.raster.gradationToShapes()
            rasterI.setShapes(circularRegion5Polygons)
            rasterI.setRasterBounds(-85.0,85.0,-85.0,85.0)
            rasterI.setRasterResolution(200,200)
            rasterI.setGradationParameters(10000.0,500000.0,30.0)
            rasterI.calculateLinearGradation()
            rasterII = qmesh.raster.gradationToShapes()
            rasterII.setShapes(circularRegion30Polygons)
            rasterII.setRasterBounds(-85.0,85.0,-85.0,85.0)
            rasterII.setRasterResolution(200,200)
            rasterII.setGradationParameters(100000.0,500000.0,20.0)
            rasterII.calculateLinearGradation()
            rasterIII = qmesh.raster.minimumRaster([rasterI, rasterII])
        except AssertionError:
            self.assert_(False)
        #Create domain object
        try:
            domain = qmesh.mesh.Domain()
            domain.setGeometry(domainLines, domainPolygons)
            domain.setMeshMetricField(rasterIII)
            domain.setTargetCoordRefSystem('EPSG:32630', fldFillValue=0.0)
        except AssertionError:
            self.assert_(False)
        #Meshing with Gmsh
        try:
            domain.gmsh(geoFilename=self.thisPath+'/'+function_name+'.geo',
                        fldFilename=self.thisPath+'/'+function_name+'.fld',
                        mshFilename=self.thisPath+'/'+function_name+'.msh',
                        gmshAlgo = 'front2d')
        except AssertionError:
            self.assert_(False)

    def test_multipleRegion_UTM31(self):
        '''Test multiple region meshing and labelling. Domain is composed of four regions, and a mesh is generated in UTM31.'''
        function_name = self.test_multipleRegion_UTM31.__name__
        #Create shapefiles
        try:
            self.create_outerRegion_shapefiles()
            self.create_circularRegion30_shapefiles()
            self.create_circularRegions5_shapefiles()
        except AssertionError:
            self.assert_(False)
        #Read-in shapefiles
        try:
            outerRegionLines = qmesh.vector.Shapes()
            outerRegionLines.fromFile(self.circularDomain_line_filename)
            outerRegionPolygons = qmesh.vector.Shapes()
            outerRegionPolygons.fromFile(self.circularDomain_polygon_filename)
            circularRegion30Lines = qmesh.vector.Shapes()
            circularRegion30Lines.fromFile(self.circularRegion30_line_filename)
            circularRegion30Polygons = qmesh.vector.Shapes()
            circularRegion30Polygons.fromFile(self.circularRegion30_polygon_filename)
            circularRegion5Lines = qmesh.vector.Shapes()
            circularRegion5Lines.fromFile(self.circularRegions5_line_filename)
            circularRegion5Polygons = qmesh.vector.Shapes()
            circularRegion5Polygons.fromFile(self.circularRegions5_polygon_filename)
        except AssertionError:
            self.assert_(False)
        #Insert smaller region into larger.
        try:
            domainLines, domainPolygons = \
               qmesh.vector.insertRegions(
                  outerRegionLines, outerRegionPolygons,\
                  circularRegion30Lines, circularRegion30Polygons)
            domainLines, domainPolygons = \
               qmesh.vector.insertRegions(
                  domainLines, domainPolygons,\
                  circularRegion5Lines, circularRegion5Polygons)
        except AssertionError:
            self.assert_(False)
        #Create gradated mesh metric raster for inserted region.
        try:
            rasterI = qmesh.raster.gradationToShapes()
            rasterI.setShapes(circularRegion5Polygons)
            rasterI.setRasterBounds(-85.0,85.0,-85.0,85.0)
            rasterI.setRasterResolution(200,200)
            rasterI.setGradationParameters(10000.0,500000.0,30.0)
            rasterI.calculateLinearGradation()
            rasterII = qmesh.raster.gradationToShapes()
            rasterII.setShapes(circularRegion30Polygons)
            rasterII.setRasterBounds(-85.0,85.0,-85.0,85.0)
            rasterII.setRasterResolution(200,200)
            rasterII.setGradationParameters(100000.0,500000.0,20.0)
            rasterII.calculateLinearGradation()
            rasterIII = qmesh.raster.minimumRaster([rasterI, rasterII])
        except AssertionError:
            self.assert_(False)
        #Create domain object
        try:
            domain = qmesh.mesh.Domain()
            domain.setGeometry(domainLines, domainPolygons)
            domain.setMeshMetricField(rasterIII)
            domain.setTargetCoordRefSystem('EPSG:32631', fldFillValue=0.0)
        except AssertionError:
            self.assert_(False)
        #Meshing with Gmsh
        try:
            domain.gmsh(geoFilename=self.thisPath+'/'+function_name+'.geo',
                        fldFilename=self.thisPath+'/'+function_name+'.fld',
                        mshFilename=self.thisPath+'/'+function_name+'.msh',
                        gmshAlgo = 'front2d')
        except AssertionError:
            self.assert_(False)


if __name__ == '__main__':
    unittest.main()
